bool WIFIinit() {
    WiFi.disconnect();
    WiFi.mode(WIFI_AP);
    WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
    String softApName = String(_ssidAP.c_str()) + "-" + String(_devID);
    WiFi.softAP(softApName.c_str(), _passwordAP.c_str(), WiFi.channel(), 0);
    Serial.println("WiFi access point created");
    Serial.print("IP address: ");
    Serial.println(IPAddress(192, 168, 4, 1));
    return true;
}
