

void initDHT(){
  dht.setup(4, DHTesp::DHT11);
}

void getTempHum(){

  float humidity = dht.getHumidity();
  float temperature = dht.getTemperature();
  Serial.print("Humidity: ");
  Serial.print(humidity, 1);
  Serial.print(" Temp: ");
  Serial.print(temperature, 1);
  Serial.print(" Temp fells like: ");
  Serial.println(dht.computeHeatIndex(temperature, humidity, false), 1);
}


float getTempIndex(){
  float humidity = dht.getHumidity();
  float temperature = dht.getTemperature();
  return dht.computeHeatIndex(temperature, humidity, false);
}

