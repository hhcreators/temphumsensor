extern "C" {
  #include<user_interface.h>
  #include <espnow.h>
}
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <FS.h>
#include <ArduinoJson.h>
#include <ESP8266httpUpdate.h>
#include "DHTesp.h"

IPAddress apIP(192, 168, 4, 1);
WiFiServer server(80);
WiFiClient client;
ESP8266WebServer HTTP(80);
File fsUploadFile;
DHTesp dht;

long _stationID = 1;
long _devID = 1;
String _devType = "";

String _ssid     = "";
String _password = "";
String _ssidAP = "";
String _passwordAP = "";

String jsonConfig = "{}";
bool configSended = false;


void setup() {
  Serial.begin(230400);
  
  Serial.println("");
  FS_init();
  Serial.println("File system started !");
  loadConfig();
  Serial.println("Config file loaded !");
  initDHT();
  Serial.println("DHT ready !");
  espNowInit();
  Serial.println("ESP-Now ready !");
}

void loop() {
  delay(dht.getMinimumSamplingPeriod());
  String temp = "";
  StaticJsonBuffer<255> jsonBuffer;
  JsonObject& json = jsonBuffer.createObject();
  json["stID"] = _stationID;
  json["dvID"] = _devID;
  json["dvTyp"] = _devType;
  json["dvVal"] = "t" + String(dht.getTemperature()) + "h" + String(dht.getHumidity());
  json["type"] = "RFS";
  json.printTo(temp);
  uint8_t data[250] ;
  temp.getBytes(data,250);
  uint8_t mac[] = {0x5C,0xCF,0x7F,0xAC,0x84,0xCD};
  int result = esp_now_send(mac, data, sizeof(data));
  Serial.print("Sending status: ");
  Serial.println(result);
  Serial.print("Data: ");
  Serial.println((char*)data);
  ESP.deepSleep(30e6);
}



