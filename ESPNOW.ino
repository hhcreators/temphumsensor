
void printMacAddress(uint8_t* macaddr) {
  Serial.print("{");
  for (int i = 0; i < 6; i++) {
    Serial.print("0x");
    Serial.print(macaddr[i], HEX);
    if (i < 5) Serial.print(',');
  }
  Serial.println("}");
}

void espNowInit() {
  if (esp_now_init()==0) {
    
    esp_now_set_self_role(ESP_NOW_ROLE_SLAVE);
    
    registerSlaves();
    
    esp_now_register_recv_cb([](uint8_t *macaddr, uint8_t *data, uint8_t len) {
    Serial.println("recv_cb");

    Serial.print("mac address: ");
    printMacAddress(macaddr);

    Serial.print("data: ");
    for (int i = 0; i < len; i++) {
      Serial.print(data[i], HEX);
    }
    Serial.println("");
  });
  
  esp_now_register_send_cb([](uint8_t* macaddr, uint8_t status) {
    Serial.println("send_cb");

    Serial.print("mac address: ");
    printMacAddress(macaddr);

    Serial.print("status = "); Serial.println(status);
  });
  
    Serial.println("direct link  init ok");
  } else {
    Serial.println("dl init failed");
    ESP.restart();
    return;
  }
}

bool registerSlaves(){
  uint8_t mac[] = {0x5C,0xCF,0x7F,0xAC,0x84,0xCD};
  esp_now_add_peer(mac, (uint8_t)ESP_NOW_ROLE_CONTROLLER, 1, NULL, 0);
  return true;
}
