// config.json
bool loadConfig() {
  File configFile = SPIFFS.open("/config.json", "r");
  if (!configFile) {
    Serial.println("Failed to open config file");
    saveConfig();
    return false;
  }
  size_t size = configFile.size();
  if (size > 1024) {
    Serial.println("Config file size is too large");
    return false;
  }
  jsonConfig = configFile.readString();
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(jsonConfig);
    _stationID = root["stationID"].as<long>();
    _devID = root["devID"].as<long>();
    _devType = root["devType"].as<String>();
    _ssidAP = root["ssidAPName"].as<String>();
    _passwordAP = root["ssidAPPassword"].as<String>();
    _ssid = root["ssidName"].as<String>();
    _password = root["ssidPassword"].as<String>();
    return true;
}

// Write to config.json
bool saveConfig() {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& json = jsonBuffer.parseObject(jsonConfig);
  json["stationID"] = _stationID;
  json["devID"] = _devID;
  json["devType"] = _devType;
  json["ssidAPName"] = _ssidAP;
  json["ssidAPPassword"] = _passwordAP;
  json["ssidName"] = _ssid;
  json["ssidPassword"] = _password;
  json.printTo(jsonConfig);
  File configFile = SPIFFS.open("/config.json", "w");
  if (!configFile) {
    Serial.println("Failed to open config file for writing");
    return false;
  }
  json.printTo(configFile);
  return true;
  }

void sendConfig(){
  jsonConfig.trim();
  //socketSend(jsonConfig);
  configSended = true;
}

